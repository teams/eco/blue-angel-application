# Blue Angel Application

This repository contains the material required for applying for the [Blue Angel](https://www.blauer-engel.de/en/) label for KDE software. The Blue Angel is an ecolabel of the German federal Government which is awarded in a lot of different product categories. End of 2019 they introduced the [Blue Angel label for software](https://www.blauer-engel.de/en/products/electric-devices/resources-and-energy-efficient-software-products/resources-and-energy-efficient-software-products) and published criteria based on energy consumption measurements and other sustainability aspects.

In March 2022 Okular was the [first ever software product](https://eco.kde.org/blog/2022-03-16-press-release-okular-blue-angel/) to receive the Blue Angel ecolabel. The criteria are Free Software friendly. Their first version only covered desktop software, so KDE and other Free Software projects were well positioned to meet the requirements. You can learn more about certifying software in the handbook ["Applying The Blue Angel Criteria To Free Software"](https://eco.kde.org/handbook/).

A big part of the criteria is the measurement of energy consumption. To get the data for this we created two open projects to collect this data and drive more energy efficient software. These projects are [FOSS Energy Efficiency Project](https://invent.kde.org/teams/eco/feep) and [KEcoLab](https://invent.kde.org/teams/eco/remote-eco-lab).

This repository tracks the effort to collect the data necessary for the application to certify our applications. The forms and supplemental information are listed here. Progress is tracked in the issues.

If you have any questions or comments please feel free to reach out to Cornelius Schumacher <schumacher@kde.org> or post on the public mailing list kde-eco-discuss@kde.org.
