This directory contains the application documents for the Blue Angel label for specific software applications. Each software application has its own directory.

An application directory contains the required documents. In the cases where the document contains free-form text, the source is a markdown file, which is converted to a PDF when creating an application release.

To create an application release do the following:

1. Edit the `VERSION` file to contain the correct version number. This is the version of the application documents for the label, not the version of the software.
2. Run `make` to create the PDF versions of the markdown documents and a versioned zip archive which includes all documents.
3. Tag the repository with `<APPNAME>-v<VERSION>`, where <APPNAME> is the name of the software application and version the content of the `VERSION` file
4. Create a release on GitLab and upload the corresponding zip archive as a release artifact.
