This is the working directory for creating the application for the Blue Angel label for Okular.

## Resources

* [Okular Website](https://okular.kde.org/) ([Sources](https://invent.kde.org/websites/okular-kde-org))
* [Okular Handbook](https://docs.kde.org/stable5/en/okular/okular/index.html) ([PDF](https://docs.kde.org/stable5/en/okular/okular/okular.pdf)) ([Sources](https://invent.kde.org/graphics/okular/-/tree/master/doc))
* [Okular Wiki page on UserBase](https://userbase.kde.org/Okular)
* [Okular on apps.kde.org](https://apps.kde.org/en/okular)
* [Okular source code](https://invent.kde.org/graphics/okular)
