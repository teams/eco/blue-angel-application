# Final Evaluation

## 3.3 Requirements at the end of the term of the contract

### Requirements

 * Declaration of compliance with the requirements in Annex 1 to the contract
 * Values for the requirements from Paragraph 3.1.1
 * Submission of a Resource Efficiency Report

#### Compliance with the requirements in Annex 1

All measurement reports for both Standard Usage Scenario and Idle Mode measurements (3.1.1.2 and 3.1.1.3) have been published at the KDE Gitlab instance: <https://invent.kde.org/teams/eco/feep/-/tree/master/measurements/okular>. No updates have changed the product in a way which affected compliance with any of the requirements under "3.1.1.1 Minimum system requirements" and "3.1.1.4 Support for the energy management system".

For the most recent measurement reports (2024-09-13), please visit: <https://invent.kde.org/teams/eco/feep/-/tree/master/measurements/okular/2024-09-13>

##### Measurements of software updates

Measurements were done on the following versions of Okular since eco-certification in March 2022; note that after v.1, Okular's version numbering jumped to v.20:

 * v.1  (point version 1.9.3, measured at UCB);
 * v.21 (point version 21.08, measured at UCB);
 * v.23 (point version 23.08.1, measured at UCB); and
 * v.24 (point version version 24.08.0, measured with KEcoLab).

Due to delays in setting up the KDE software measurement lab, including CoViD restrictions on in-person meetings in winter of 2021/2022, no measurements were carried out on Okular versions 20/22.

For the first three measurements, the Reference System is a 2019 Fujitsu Esprimo P958, measured with the Janitza UMG 604 power meter. Of these, the first two were measured on a GNU/Linux operating system (Ubuntu), and the last measured on Windows (Windows 11). The reason for the change in operating systems is due to problems re-running the original Actiona script necessary for emulating user behavior. The measurements were carried out at the Umwelt Campus Birkenfeld (UCB) laboratory.

For the most recent measurement (2024-09-13), the Reference System is a 2019 Fujitsu Esprimo P957 Tower, measured with the recommended Gude Expert Power Control 1202 series power meter. The system under test is a GNU/Linux operating system (KDE Neon), and the script for emulating user behavior was a Bash script using `xdotool` commands. The measurements were carried out at the KDAB Berlin laboratory, accessible remotely via KDE's Gitlab instance with the KEcoLab software stack.

##### Energy demands remain under 10%

**Note**: There was a calibration error for the first measurement, discovered by colleagues after publication of the report. The error was due to an incorrect instrument transformer setting. The ratio of the data did not change across measurements, and those initial measurements are therefore still meaningful, but given the difference in absolute values this measurement is omitted from the list below.

The power and energy consumption measurements for the Standard Usage Scenario / Idle Mode are listed below; it is worth noting the similarity of the values for the measurements on comparable operating systems (v.21 and v.24, both measured on Ubuntu-based systems), despite differences in the lab setup and emulation tools used:

 * v.21: SUS power 12,75 W, energy consumption 0,77 Wh / Idle Mode power 11,33 W, energy consumption 0,68 Wh
 * v.23: SUS power 13,72 W, energy consumption 0,82 Wh / Idle Mode na
 * v.24: SUS power 12,794 W, energy consumption 0,782 Wh / Idle Mode power 12,073 W, energy consumption 0,738 Wh

Thus, the power and energy consumption did *not* increase by more than 10% during the certification period. With the v.21 measurement as a reference point, the values for the SUS remain under the threshold of 14,025 W and 0,847 Wh, and for the Idle mode the values remain under 12,463 W and 0,748 Wh.

#### Resource Efficiency Report

The were many and varied efforts taken in the KDE community to increase energy and resource efficiency. These include efficiency improvements to Okular and other KDE software, summarized below in terms of Merge Requests labelled "Efficiency" (numbers as of 2024-09-18). The vast majority of these Merge Requests have been merged into KDE's software products; links to the relevant group repositories providing a complete list are provided below.

 * Okular: "Use QStringView more to reduce allocations" <https://invent.kde.org/graphics/okular/-/merge_requests/926>
 * KDE Graphics applications: 15 additional efficiency Merge Requests <https://invent.kde.org/groups/graphics/-/merge_requests?scope=all&state=all&label_name[]=Efficiency>
 * KDE Plasma: 429 efficiency Merge Requests <https://invent.kde.org/groups/plasma/-/merge_requests?scope=all&state=all&label_name[]=Efficiency>
 * KDE Education: 4 efficiency Merge Requests <https://invent.kde.org/groups/education/-/merge_requests?scope=all&state=all&label_name[]=Efficiency>
 * KDE Operating System components: 10 efficiency Merge Requests <https://invent.kde.org/groups/system/-/merge_requests?scope=all&state=all&label_name[]=Efficiency>
 * KDE SDK: 5 efficiency Merge Requests <https://invent.kde.org/groups/sdk/-/merge_requests?scope=all&state=all&label_name[]=Efficiency>
 * KDE Utilities: 6 efficiency Merge Requests <https://invent.kde.org/groups/utilities/-/merge_requests?scope=all&state=all&label_name[]=Efficiency>
 * KDE PIM: 4 efficiency Merge Requests <https://invent.kde.org/groups/pim/-/merge_requests?scope=all&state=all&label_name[]=Efficiency>
 * KDE Frameworks: 105 efficiency Merge Requests <https://invent.kde.org/groups/frameworks/-/merge_requests?scope=all&state=opened&label_name[]=Efficiency>
 * KDE Networks: 18 efficiency Merge Requests <https://invent.kde.org/groups/network/-/merge_requests?scope=all&state=all&label_name[]=Efficiency>
 * KDE Multimedia: 2 efficiency Merge Requests <https://invent.kde.org/groups/multimedia/-/merge_requests?scope=all&state=all&label_name[]=Efficiency>
 * KDE Plasma Bigscreen: 1 efficiency Merge Request <https://invent.kde.org/groups/plasma-bigscreen/-/merge_requests?scope=all&state=all&label_name[]=Efficiency>

The KDE communnity has undertaken projects to develop internal tooling for measuring the energy consumption of KDE software and other Free Software projects. These include:

 * KEcoLab Measurement Lab: 3 lab setup sprints, 1 Google Summer of Code project, 3 Season of KDE projects
 * Emulation tooling with KdeEcoTest / Selenium: 4 Season of KDE projects
 * Emulation script preparation: 2 Season of KDE projects

Additionally, there were several outreach measures both within KDE as well as within adjacent Free Software and environmental communities, including monthly meetups, participation in local workshops, and presentations at international conferences:

 * Within KDE community: monthly meetups since September 2021; community adoption of a Sustainable Software goal; 8 internal presentations (Akademy, Linux App Summit, KDE India Conference, etc.)
 * Within FOSS communities: 13 presentations (Chaos Computer Congress, SFSCon, Ubuntu Summit, etc.)
 * Within digital sustainability communities: 7 presentations (COP 26, Nachhaltig By Design, Bits und Bäume, etc.)
 * For a full list, please visit (after 2021): https://community.kde.org/Promo/Events/Historic

Outreach about software's resource and energy efficiency also included written texts, such as the KDE Eco handbook for software developers "Applying The Blue Angel Criteria To Free Software" (https://eco.kde.org/handbook/), regular blog posts (https://eco.kde.org/blog), an active social media presence (https://floss.social/@be4foss), and co-authorship of a peer-reviewed academic article:

 * Guldner et al. (2024). "Development and evaluation of a reference measurement model for assessing the resource and energy efficiency of software products and components—Green Software Measurement Model (GSMM)", Future Generation Computer Systems. https://doi.org/10.1016/j.future.2024.01.033
