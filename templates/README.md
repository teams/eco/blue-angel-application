These are templates for collecting the data which needs to be filled into the application form and its annexes. It contains of two parts, a template from which issues are generated to track filling out the information, and a set of files, which correspond to the annexes where the data is filled in.

The templates are used by copying the form templates to the directory for the application under `applications/<name of application>` and running the script `create_issues <name of application>` to create the issues on GitLab.

* Annex 1: Information and compliance with the criteria based on [docx template](../resources/annexes/DE-UZ 215-eng Annex 1.docx)
* Annex 2: Spreadsheet file with measurement information based on [xlsx template](../resources/annexes/DE-UZ 215-eng Annex 2.xlsx)
* Annex 3: Measurement reports. These are the two OSCAR-Reports for the idle and the standard usage scenario
* [Annex 4](form/annex-4.md): Proof documents of the data formats
* [Annex 5](form/annex-5.md): Verification documents (Interface documentation, permalink to the software source code on a source code management platform, software licences or similar)
* [Annex 6](form/annex-6.md): Product information
* Annex 7: Measurement data in defined XML (see Appendix C)
