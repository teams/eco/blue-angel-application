#!/usr/bin/env python3

import sys
from pathlib import Path
from openpyxl import load_workbook
from xml.dom import minidom

if len(sys.argv) != 2:
    print("Usage create_annex_7.py <appname>", file=sys.stderr)
    sys.exit(1)

app_name = sys.argv[1]

app_dir = Path(__file__).resolve().parent.parent / "applications" / app_name

annex2 = app_dir / ("de-uz-215-eng-annex-2-" + app_name + ".xlsx")
annex7 = app_dir / ("de-uz-215-eng-annex-7-" + app_name + ".xml")

wb = load_workbook(annex2)

ws = wb["Requirements"]

fields = {}

for row_id, row in enumerate(ws.values):
    for col_id, field in enumerate(row):
        if field:
            fields[(row_id, col_id)] = field

def field(row,col):
    row_id = row - 1
    col_id = ord(col) -  65
    if (row_id, col_id) in fields:
        return fields[(row_id,col_id)]
    else:
        return ""

import xml.etree.cElementTree as ET

root = ET.Element("Blue-Angel-Software-Criteria")

product = ET.SubElement(root, "Product")
ET.SubElement(product, "ProductName").text = field(15, "C")
ET.SubElement(product, "ProductVersion").text = field(16, "C")
ET.SubElement(product, "ProductManufacturer").text = field(17, "C")
ET.SubElement(product, "SoftwareArchitecture").text = field(18, "C")

personnel = ET.SubElement(root, "MeasurementPersonnelData")
ET.SubElement(personnel, "MeasurementDate").text = field(21, "C")
sender = ET.SubElement(personnel, "MeasurementSender")
ET.SubElement(sender, "MeasurementSenderName").text = field(24, "C")
ET.SubElement(sender, "MeasurementSenderMail").text = field(25, "C")
ET.SubElement(sender, "MeasurementSenderOther").text = field(26, "C")
institute = ET.SubElement(personnel, "MeasurementSenderInstitute")
ET.SubElement(institute, "InstituteName").text = field(29, "C")
ET.SubElement(institute, "InstituteAddress").text = field(30, "C")
ET.SubElement(institute, "InstituteWebsite").text = field(31, "C")
ET.SubElement(institute, "InstituteOther").text = field(32, "C")
ET.SubElement(personnel, "MeasurementNote").text = field(34, "C")

technical_data = ET.SubElement(root, "MeasurementTechnicalData")
ET.SubElement(technical_data, "MeasurementDevice").text = field(15, "G")
ET.SubElement(technical_data, "SamplingFrequency").text = field(16, "G")
ET.SubElement(technical_data, "ScenarioLength").text = field(17, "G")
ET.SubElement(technical_data, "SamplingSize").text = field(18, "G")

measurement_sut = ET.SubElement(root, "MeasurementSUTDetails")
ET.SubElement(measurement_sut, "MeasurementReferenceSystemYear").text = field(23, "G")
ET.SubElement(measurement_sut, "MeasurementSUTSystem").text = field(24, "G")
ET.SubElement(measurement_sut, "MeasurementSUTModel").text = field(25, "G")
ET.SubElement(measurement_sut, "MeasurementSUTProcessor").text = field(26, "G")
ET.SubElement(measurement_sut, "MeasurementSUTCores").text = field(27, "G")
ET.SubElement(measurement_sut, "MeasurementSUTClockSpeed").text = field(28, "G")
ET.SubElement(measurement_sut, "MeasurementSUTRAM").text = field(29, "G")
ET.SubElement(measurement_sut, "MeasurementSUTHardDisk").text = field(30, "G")
ET.SubElement(measurement_sut, "MeasurementSUTGraphicsCard").text = field(31, "G")
ET.SubElement(measurement_sut, "MeasurementSUTNetwork").text = field(32, "G")
ET.SubElement(measurement_sut, "MeasurementSUTCache").text = field(33, "G")
ET.SubElement(measurement_sut, "MeasurementSUTMainboard").text = field(34, "G")
ET.SubElement(measurement_sut, "MeasurementSUTOperatingSystem").text = field(35, "G")

compat_sut = ET.SubElement(root, "BackwardCompatibilitySUTDetails")
ET.SubElement(compat_sut, "BackwardReferenceSystemYear").text = field(23, "H")
ET.SubElement(compat_sut, "BackwardSUTSystem").text = field(24, "H")
ET.SubElement(compat_sut, "BackwardSUTModel").text = field(25, "H")
ET.SubElement(compat_sut, "BackwardSUTProcessor").text = field(26, "H")
ET.SubElement(compat_sut, "BackwardSUTCores").text = field(27, "H")
ET.SubElement(compat_sut, "BackwardSUTClockSpeed").text = field(28, "H")
ET.SubElement(compat_sut, "BackwardSUTRAM").text = field(29, "H")
ET.SubElement(compat_sut, "BackwardSUTHardDisk").text = field(30, "H")
ET.SubElement(compat_sut, "BackwardSUTGraphicsCard").text = field(31, "H")
ET.SubElement(compat_sut, "BackwardSUTNetwork").text = field(32, "H")
ET.SubElement(compat_sut, "BackwardSUTCache").text = field(33, "H")
ET.SubElement(compat_sut, "BackwardSUTMainboard").text = field(34, "H")
ET.SubElement(compat_sut, "BackwardSUTOperatingSystem").text = field(35, "H")

stack = ET.SubElement(root, "SoftwareStack")
ET.SubElement(stack, "SoftwareStackData").text = field(38, "B")


resource_efficiency = ET.SubElement(root, "ResourceEfficiency")
hardware_efficiency = ET.SubElement(resource_efficiency, "HardwareEfficiency")


def add_indicator(element, id, name, note1, result, unit, note2):
    indicator = ET.SubElement(element, "Indicator")
    ET.SubElement(indicator, "IndicatorID").text = id
    ET.SubElement(indicator, "IndicatorName").text = name
    ET.SubElement(indicator, "IndicatorNote").text = note1
    ET.SubElement(indicator, "IndicatorResult").text = result
    ET.SubElement(indicator, "IndicatorUnit").text = unit
    ET.SubElement(indicator, "IndicatorNote").text = note2

def add_indicator_fields(element, row, col):
    id = field(row, col)
    name = field(row, chr(ord(col) + 1))
    note1 = field(row, chr(ord(col) + 2))
    result = field(row, chr(ord(col) + 3))
    unit = field(row, chr(ord(col) + 4))
    note2 = field(row, chr(ord(col) + 5))
    add_indicator(element, id, name, note1, result, unit, note2)

requirements = ET.SubElement(hardware_efficiency, "MinimumSystemRequirements")
add_indicator_fields(requirements, 52, "C")
add_indicator_fields(requirements, 53, "C")
add_indicator_fields(requirements, 54, "C")
add_indicator_fields(requirements, 55, "C")
add_indicator_fields(requirements, 56, "C")
add_indicator_fields(requirements, 57, "C")

idle = ET.SubElement(hardware_efficiency, "HWLoadIdle")
add_indicator_fields(idle, 60, "C")
add_indicator_fields(idle, 61, "C")
add_indicator_fields(idle, 62, "C")
add_indicator_fields(idle, 63, "C")
add_indicator_fields(idle, 64, "C")

utilisation = ET.SubElement(hardware_efficiency, "HWUtilisationEnergyDemand")
add_indicator(utilisation, "x", "x", "x", "x", "x", "x")
add_indicator_fields(utilisation, 68, "C")
add_indicator_fields(utilisation, 69, "C")
add_indicator_fields(utilisation, 70, "C")
add_indicator_fields(utilisation, 71, "C")
add_indicator_fields(utilisation, 72, "C")

life = ET.SubElement(root, "HWOperatingLife")
compat = ET.SubElement(life, "BackwardCompatibility")
year = ET.SubElement(compat, "BackwardCompatibilityYear")
add_indicator_fields(year, 76, "C")


xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent="  ")

with annex7.open("w") as f:
  f.write(xmlstr)
