#!/usr/bin/env python3

from graphql import Mutation
from api import Api
from issue_templates import IssueTemplates

import sys
from pathlib import Path

if len(sys.argv) != 2:
    print("Usage: create_issues.py <name of application>", file=sys.stderr)
    sys.exit(1)

application = sys.argv[1]

templates = IssueTemplates(application)

path = Path(__file__).resolve().parent.parent / "templates" / "issues.md"
templates.read(path)

api = Api("https://invent.kde.org/api/graphql", "cschumac", "blue-angel-application")

for issue in templates.issues:
    print("Creating issue: ", issue.title)
    api.create_issue(issue.title, issue.description)
