#!/usr/bin/env python3

from api import Api

api = Api("https://invent.kde.org/api/graphql", "cschumac", "blue-angel-application")

result = api.get_issues()

for issue in result["project"]["issues"]["nodes"]:
    print(f"TITLE {issue['title']}")
    print(issue["description"])
    print("")
