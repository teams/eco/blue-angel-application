#!/usr/bin/env python3

class Issue:
    def __init__(self):
        self.title = ""
        self.description = ""

class IssueTemplates:
    def __init__(self, application):
        self.application = application

    def add_issue(self, issue):
        if issue:
            issue.description += f"\n/milestone {self.application}\n"
            self.issues.append(issue)

    def read(self, path):
        self.issues = []
        issue = None
        with path.open() as f:
            for line in f:
                if line.startswith("# "):
                    self.add_issue(issue)
                    issue = Issue()
                    issue.title = line[2:].strip()
                else:
                    issue.description += line
        self.add_issue(issue)
        return self.issues
