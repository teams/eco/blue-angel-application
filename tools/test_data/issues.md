# 3.1.1.3: Hardware utilisation and energy demand when running a standard scenario

/label ~"Annex 1" ~"Annex 2" ~"Annex 3"

**Requirement**

* [ ] Compliance with the requirement is confirmed.

**Documentation**

* [ ] Spreadsheet file (Annex 2)
* [ ] Measurement report (Annex 3)

# 3.1.1.4: Support for the energy management system

/label ~"Annex 1"

**Requirement**: The software product must be capable of unrestricted functional use with activated energy management of the underlying system layers or the connected client systems.

* [ ] Compliance with the requirement is confirmed.
